const user = require('../../src/user');

it('renders correctly', () => {
    expect(user(1)).toMatchSnapshot();
    expect(user(56)).toMatchSnapshot();
    expect(user(1345)).toMatchSnapshot();
});

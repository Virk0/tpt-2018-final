var config = require('../../nightwatch.conf.js');

module.exports = {
    'Google Tallinn': function(browser) {
        browser
            .resizeWindow(1280, 800)
            .url('https://google.ee/')
            .waitForElementVisible('body div#main')
            .setValue('input[class="gLFyf gsfi"]', ['tallinn', browser.Keys.ENTER])
            .assert.containsText('body', 'tallinn')
            .saveScreenshot(config.imgpath(browser) + 'tallinnSearch.png')
            .pause(2000)
            .click('h3[class="LC20lb"]')
            .saveScreenshot(config.imgpath(browser) + 'tallinnFirstPage.png')
            .pause(2000)
            .end();
    }
};
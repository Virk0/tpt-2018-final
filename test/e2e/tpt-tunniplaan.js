var config = require('../../nightwatch.conf.js');

module.exports = {
    'tptlive tunniplaan': function(browser) {
        browser
            .resizeWindow(1280, 800)
            .url('https://www.tptlive.ee//')
            .pause (2000)
            .saveScreenshot(config.imgpath(browser) + 'frontpage.png')
            .click('li[id="menu-item-1313"]')
            .pause (2000)
            .saveScreenshot(config.imgpath(browser) + 'tunniplaan.png')
            .click('a[href="https://tpt.siseveeb.ee/veebivormid/tunniplaan/tunniplaan?oppegrupp=226&nadal=03.12.2018"]')
            .pause(2000)
            .saveScreenshot(config.imgpath(browser) + 'TA-17E tunniplaan.png')
            .end();
    }
};